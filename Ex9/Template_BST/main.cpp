#include <iostream>
#include "BSNode.h"

using std::string;

#define ARR_LENGTH 15

template <class T>
void printArr(T arr[], const int n) {
	for (int i = 0; i < n; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}

int main() {
	int numArr[ARR_LENGTH] = { 10, 14, 28, 11, 7, 16, 30, 50, 25, 18, 2, 5, 6, 3, 99 };
	string strArr[ARR_LENGTH] = { "foo", "ab", "vs", "bd", "gr", "bgf", "hy", "h", "bb", "aa", "zgtt", "ff", "gtr", "fg", "kuy" };
	BSNode<int>* numBST = new BSNode<int>(numArr[0]);
	BSNode<string>* strBST = new BSNode<string>(strArr[0]);

	// printing before sort
	printArr<int>(numArr, ARR_LENGTH);
	printArr<string>(strArr, ARR_LENGTH);

	// inserting values to the tree
	for (int i = 1; i < ARR_LENGTH; i++) {
		numBST->insert(numArr[i]);
		strBST->insert(strArr[i]);
	}

	std::cout << "number array sorted:" << std::endl;
	numBST->printNodes();
	std::cout << "string array sorted:" << std::endl;
	strBST->printNodes();

	delete numBST;
	delete strBST;
	return 0;
}