#pragma once
#pragma comment(lib, "printTreeToFile")

#include <string>
#include "BSNode.h"
void printTreeToFile(const BSNode* bs, std::string output);
