#include "BSNode.h"

BSNode::BSNode(std::string data)
{
	_data = data;
	_count = 1;
	_right = nullptr;
	_left = nullptr;
}

BSNode::BSNode(const BSNode& other) : _left(nullptr), _right(nullptr), _data(other._data), _count(other._count)
{
	if (other._left) {
		_left = new BSNode(*other._left);
	}
	if (other._right) {
		_right = new BSNode(*other._right);
	}
}

BSNode::~BSNode()
{
	// recursivly deletes all nodes
	if (_left) {
		delete _left;
	}
	if (_right) {
		delete _right;
	}
}


void BSNode::insert(std::string value)
{ // assuming the root is not empty
	if (value > _data) { // inserting the big value to right
		if (_right) {
			_right->insert(value);
		}
		else {
			_right = new BSNode(value);
		}
	}
	else if(value < _data) // inserting the small value to left
	{
		if (_left) {
			_left->insert(value);
		}
		else {
			_left = new BSNode(value);
		}
	}
	else {
		_count++;
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	if (this == &other) {
		return *this;
	}
	BSNode temp(other); // copying new data (other) to temp
	// swapping between old data and new data
	std::swap(temp._right, _right);
	std::swap(temp._left, _left);
	std::swap(temp._data, _data);
	std::swap(temp._count, _count);
	return *this;	// when function reaches end, the old data is realesed and deleted
}

bool BSNode::isLeaf() const
{
	if (_right == nullptr && _left == nullptr) { // leaf has no sons
		return true;
	}
	return false;
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return _left;
}

BSNode* BSNode::getRight() const
{
	return _right;
}

bool BSNode::search(std::string val) const
{
	if (this == nullptr) { // reached end - didn't find
		return false;
	}
	else if (val > _data) {
		return _right->search(val);
	}
	else if (val < _data) {
		return _left->search(val);
	}

	return true; // found equal value
}

int BSNode::getHeight() const
{
	if (this == nullptr) {
		return -1; // returning -1 so that the level of root is 0 (-1 + 1 = 0)
	}

	return 1 + std::max(_left->getHeight(), _right->getHeight()); // max height from each son
}

int BSNode::getDepth(const BSNode& root) const
{
	if (root.search(this->_data)) { // checking if the value exists in the root
		return root.getCurrNodeDistFromInputNode(this);
	}
	return -1;
}

void BSNode::printNodes() const
{ // first printing left side, then root, and then the right side
	if (this == nullptr) {
		return;
	}
	_left->printNodes();
	std::cout << _data << " " << _count << std::endl;
	_right->printNodes();
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (this->_data > node->_data) {
		return 1 + _left->getCurrNodeDistFromInputNode(node);
	}
	else if (this->_data < node->_data) {
		return 1 + _right->getCurrNodeDistFromInputNode(node);
	}
	return 0; // starting the count of depth from 0 when reaches value
}
