#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
#pragma comment(lib, "printTreeToFile")

using std::cout;
using std::endl;

int main()
{
	BSNode* a = new BSNode("5");
	a->insert("5");
	a->insert("2");
	a->insert("2");
	a->insert("9");
	a->insert("9");
	a->insert("1");
	a->insert("1");
	a->insert("8");
	a->insert("8");

	BSNode* b = new BSNode("6");
	*b = *a;
	b->printNodes();
	cout << "Tree height: " << b->getHeight() << endl;
	cout << "depth of node with 3 depth: " << b->getDepth(*b->getLeft()) << endl << endl; // should return -1


	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");


	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;


	std::string textTree = "BSTData.txt";
	printTreeToFile(b, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;
	delete a;
	delete b;

	return 0;
}

