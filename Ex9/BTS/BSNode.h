#pragma once

#include <string>
#include <iostream>
#include <algorithm>
#include <math.h>


class BSNode
{
public:
	BSNode(std::string data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(std::string value); // inserting a string value to the BST
	BSNode& operator=(const BSNode& other); 

	bool isLeaf() const;
	std::string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(std::string val) const; // searching a value in the tree

	int getHeight() const; // calculating height of tree
	int getDepth(const BSNode& root) const; // depth of current node from the root given

	void printNodes() const; //for question 1 part C

private:
	std::string _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth
};