#pragma once

#include <iostream>
#include <math.h>

template <typename T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	virtual ~BSNode();

	virtual BSNode* insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const;

	T _data;
	BSNode* _left;
	BSNode* _right;

protected:

	int _count;
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth
};


template<typename T>
BSNode<T>::BSNode(T data)
{
	_data = data;
	_count = 1;
	_right = nullptr;
	_left = nullptr;
}

template<typename T>
BSNode<T>::BSNode(const BSNode& other) : _left(nullptr), _right(nullptr), _data(other._data), _count(other._count)
{
	if (other._left) {
		_left = new BSNode(*other._left);
	}
	if (other._right) {
		_right = new BSNode(*other._right);
	}
}

template<typename T>
BSNode<T>::~BSNode()
{
	// recursivly deletes all nodes
	if (_left) {
		delete _left;
	}
	if (_right) {
		delete _right;
	}
}

template<typename T>
BSNode<T>* BSNode<T>::insert(T value)
{ // assuming the root is not empty
	if (value > _data) { // inserting the big value to right
		if (_right) {
			_right->insert(value);
		}
		else {
			_right = new BSNode(value);
		}
	}
	else if (value < _data) // inserting the small value to left
	{
		if (_left) {
			_left->insert(value);
		}
		else {
			_left = new BSNode(value);
		}
	}
	else {
		_count++;
	}
	return this;
}

template<typename T>
BSNode<T>& BSNode<T>::operator=(const BSNode<T>& other)
{
	if (this == &other) {
		return *this;
	}
	BSNode temp(other); // copying new data to temp
	// swapping between old data and new data
	std::swap(temp._right, _right);
	std::swap(temp._left, _left);
	std::swap(temp._data, _data);
	std::swap(temp._count, _count);
	return *this;	// when function reaches end, the old data is realesed and deleted
}

template<typename T>
bool BSNode<T>::isLeaf() const
{
	if (_right == nullptr && _left == nullptr) {
		return true;
	}
	return false;
}

template<typename T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template<typename T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return _left;
}

template<typename T>
BSNode<T>* BSNode<T>::getRight() const
{
	return _right;
}

template<typename T>
bool BSNode<T>::search(T val) const
{
	if (this == nullptr) { // reached end
		return false;
	}
	else if (val > _data) {
		return _right->search(val);
	}
	else if (val < _data) {
		return _left->search(val);
	}

	return true; // found equal value
}

template<typename T>
int BSNode<T>::getHeight() const
{
	if (this == nullptr) {
		return -1; // returning -1 so that the level of root is 0 (-1 + 1 = 0)
	}

	return 1 + std::max(_left->getHeight(), _right->getHeight());
}

template<typename T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	if (root.search(this->_data)) {
		return root.getCurrNodeDistFromInputNode(this);
	}
	return -1;
}

template<typename T>
void BSNode<T>::printNodes() const
{ // first printing left side, then root, and then the right side
	if (this == nullptr) {
		return;
	}
	_left->printNodes();
	std::cout << _data << " " << _count << std::endl;
	_right->printNodes();
}

template<typename T>
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (this->_data > node->_data) {
		return 1 + _left->getCurrNodeDistFromInputNode(node);
	}
	else if (this->_data < node->_data) {
		return 1 + _right->getCurrNodeDistFromInputNode(node);
	}
	return 0; // starting the count of depth from 0 when reaches value
}
