#pragma once

#include "BSNode.h"
#include <math.h>
#include <execution>

template <typename T>
class AVLTree : public BSNode<T>
{
public:
	AVLTree(T data);
	~AVLTree();

	AVLTree<T>* insert(T value); // inserting values in a balanced way
private:
	int getBalanceFactor() const; // getting balance of current root
	AVLTree<T>* rotateRight(); // left rotating subtree rooted with the head root
	AVLTree<T>* rotateLeft(); // right rotating subtree rooted with the head root
};

template<typename T>
inline AVLTree<T>::AVLTree(T data) : BSNode<T>(data)
{
}

template<typename T>
inline AVLTree<T>::~AVLTree()
{
}

template<typename T>
inline AVLTree<T>* AVLTree<T>::insert(T value)
{
	if (value > this->_data) { // inserting the big value to right
		// if not empty
		(this->_right) ? this->_right = this->_right->insert(value) : this->_right = new AVLTree<T>(value);
	}
	else if (value < this->_data) // inserting the small value to left
	{	// if not empty
		(this->_left) ? this->_left = this->_left->insert(value) : this->_left = new AVLTree<T>(value);
	}
	else {
		throw std::invalid_argument("value exists");
	}

	int balance = this->getBalanceFactor(); // balance = lHeight - rHeight

	if (balance > 1 && value < this->_left->_data) // LL
		return this->rotateRight();

	if (balance < -1 && value > this->_right->_data) // RR
		return this->rotateLeft();
  
	if (balance > 1 && value > (this->_left)->_data) // LR
	{
		this->_left = ((AVLTree<T>*)this->_left)->rotateLeft();
		return this->rotateRight();
	}
 
	if (balance < -1 && value < (this->_right)->_data) // RL
	{
		this->_right = ((AVLTree<T>*)this->_right)->rotateRight();
		return this->rotateLeft();
	}

	return this;
}

template<typename T>
inline int AVLTree<T>::getBalanceFactor() const
{
	if (this == nullptr) {
		return 0;
	}
	return this->_left->getHeight() - this->_right->getHeight();
}

template<typename T>
inline AVLTree<T>* AVLTree<T>::rotateRight()
{
	BSNode<T>* x = this->_left;
	BSNode<T>* temp = x->_right;

	// rotating
	x->_right = this;
	this->_left = temp;

	return (AVLTree<T>*)x; // new root after rotation
}

template<typename T>
inline AVLTree<T>* AVLTree<T>::rotateLeft()
{
	BSNode<T>* x = this->_right;
	BSNode<T>* temp = x->_left;

	// rotating
	x->_left = this;
	this->_right = temp;

	return (AVLTree<T>*)x; // new root after rotation
}
