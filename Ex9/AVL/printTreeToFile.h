#pragma once

#include <fstream>
#include <string>
#include <sstream>
#include "BSNode.h"

template<typename T>
std::string toTstring(T a) { // converting any type to a string
	std::stringstream s;
	s << a;
	return s.str();
}

template<typename T>
void printTreeToFile(const BSNode<T>* bs, std::string output) {
	std::ofstream myfile(output);
	if (myfile.is_open()) {
		std::string data;
		getPreorder(bs, data); // getting the values of the tree in a preorder
		myfile << data; // writing to file
		myfile.close();
	}
	else std::cout << "Unable to open file";
}

// function uses preorder to update a string with the tree's values
template<typename T>
void getPreorder(const BSNode<T>* root, std::string& data) {
	if (root == nullptr) {
		data += "# ";
		return;
	}

	data += toTstring(root->getData()) + " "; // adding value to string

	getPreorder(root->getLeft(), data);
	getPreorder(root->getRight(), data);
}