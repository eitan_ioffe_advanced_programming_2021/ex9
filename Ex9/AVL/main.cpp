#include <iostream>
#include "BSNode.h"
#include "AVLTree.h"
#include "printTreeToFile.h"

using std::string;

int main() {
	AVLTree<int>* a = new AVLTree<int>(5); // inserting 7 values - should balance them
	a = a->insert(8);
	a = a->insert(4);
	a = a->insert(6);
	a = a->insert(2);
	a = a->insert(3);
	a = a->insert(10);

	AVLTree<int> b(0);
	b = *a;
	a = a->insert(43);
	try {
		a = a->insert(43);
	}
	catch (const std::exception& exc)
	{
		// catch anything thrown within try block that derives from std::exception
		std::cerr << exc.what() << std::endl;
	}

	a->printNodes();
	b.printNodes();
	string textTree = "BSTData.txt";
	printTreeToFile(&b, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());

	delete a;

	return 0;
}