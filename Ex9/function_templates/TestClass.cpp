#include "TestClass.h"

TestClass::TestClass(const int a)
{
	_val = a;
}

TestClass::~TestClass()
{
}

bool TestClass::operator<(const TestClass& other) const
{
	return (_val < other._val);
}

bool TestClass::operator==(const TestClass& other) const
{
	return (_val == other._val);
}

std::ostream& operator<<(std::ostream& os, const TestClass& f)
{
	os << f._val;
	return os;
}
