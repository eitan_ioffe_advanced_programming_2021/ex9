#pragma once
#include <iostream>

#define FIRST_IS_BIGGER -1
#define SECOND_IS_BIGGER 1
#define EQUAL 0

template <class T>
int compare(const T& a, const T& b) {
	if (a == b) {
		return EQUAL;
	}
	else if (a < b)
	{
		return SECOND_IS_BIGGER;
	}
	return FIRST_IS_BIGGER;
}

template <class T>
void bubbleSort(T arr[], const int n) {
	for (int i = 0; i < n; i++) {
		for (int j = 1; j < n; j++) {
			if (arr[j] < arr[j - 1]) {
				std::swap(arr[j], arr[j - 1]);
			}
		}
	}
}

template <class T>
void printArray(const T arr[], const int n) {
	for (int i = 0; i < n; i++) {
		std::cout << arr[i] << std::endl;
	}
}
