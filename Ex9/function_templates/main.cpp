#include "functions.h"
#include "TestClass.h"
#include <iostream>

#define ARR_SIZE 5

int main() {
    //checking on chars
    std::cout << "correct print is 1 -1 0" << std::endl;
    std::cout << compare<char>('a', 'b') << std::endl;
    std::cout << compare<char>('r', 'e') << std::endl;
    std::cout << compare<char>('b', 'b') << std::endl;

    //check bubbleSort
    std::cout << "correct print is sorted array" << std::endl;
    const int arr_size = 5;
    char doubleArr[arr_size] = { 'c', 'd', 'a', 'e', 'b' };
    bubbleSort<char>(doubleArr, arr_size);
    for (int i = 0; i < arr_size; i++) {
        std::cout << doubleArr[i] << " ";
    }
    std::cout << std::endl;

    //check printArray
    std::cout << "correct print is sorted array" << std::endl;
    printArray<char>(doubleArr, arr_size);
    std::cout << std::endl;

	// checking on a class
	TestClass a(1);
	TestClass b(2);
	TestClass c(3);
	TestClass d(4);
	TestClass e(5);

    std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<TestClass>(a, b) << std::endl;
	std::cout << compare<TestClass>(b, a) << std::endl;
	std::cout << compare<TestClass>(a, a) << std::endl;

    std::cout << "correct print is sorted array" << std::endl;
	TestClass arr[ARR_SIZE] = {a, e, c, b, d};
	bubbleSort<TestClass>(arr, ARR_SIZE);
	printArray<TestClass>(arr, ARR_SIZE);

	system("pause");
	return 0;
}
