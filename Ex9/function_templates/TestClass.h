#pragma once
#include <iostream>

class TestClass {
public:
	TestClass(const int a);
	~TestClass();

	bool operator<(const TestClass& other) const;
	bool operator==(const TestClass& other) const;
	friend std::ostream& operator<<(std::ostream& os, const TestClass& f);
private:
	int _val;
};